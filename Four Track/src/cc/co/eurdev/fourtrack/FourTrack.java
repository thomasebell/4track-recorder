package cc.co.eurdev.fourtrack;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;
import cc.co.eurdev.fourtrack.widget.VerticalSeekBar;

public class FourTrack extends Activity {
    /** Called when the activity is first created. */
	
	String trackPath;
	boolean sdIsMounted;
	LinearLayout layout;
	LinearLayout.LayoutParams params;
	AudioRecorder ar;
	HashMap<Integer, String> trackMap;
	boolean track1Exists;
	boolean track2Exists;
	boolean track3Exists;
	boolean track4Exists;
	
	MediaPlayer mp1;
	MediaPlayer mp2;
	MediaPlayer mp3;
	MediaPlayer mp4;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mp1 = new MediaPlayer();
    	mp2 = new MediaPlayer();
    	mp3 = new MediaPlayer();
    	mp4 = new MediaPlayer();
        this.trackPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
							"/Android/data/" + getString(R.string.package_name) + "/";

        this.sdIsMounted = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        
        trackMap = new HashMap<Integer, String>(4);
        
        if (this.trackMap.isEmpty()) {
        	this.trackMap.put(R.id.radio0, "track1.wav");
        	this.trackMap.put(R.id.radio1, "track2.wav");
        	this.trackMap.put(R.id.radio2, "track3.wav");
        	this.trackMap.put(R.id.radio3, "track4.wav");
        }
        

        
        layout = (LinearLayout)findViewById(R.id.volumeControls);
        params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT, 1f);
        //layout.setGravity(Gravity.CENTER_HORIZONTAL);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        
        VerticalSeekBar verticalSeekBar1 = new VerticalSeekBar(this);
        VerticalSeekBar verticalSeekBar2 = new VerticalSeekBar(this);
        VerticalSeekBar verticalSeekBar3 = new VerticalSeekBar(this);
        VerticalSeekBar verticalSeekBar4 = new VerticalSeekBar(this);
        
        Drawable thumb = getResources().getDrawable(R.drawable.empty);
        
        thumb.mutate().setAlpha(0);
        
        verticalSeekBar1.setThumb(thumb);
        verticalSeekBar2.setThumb(thumb);
        verticalSeekBar3.setThumb(thumb);
        verticalSeekBar4.setThumb(thumb);
        
        layout.addView(verticalSeekBar1, params);
        layout.addView(verticalSeekBar2, params);
        layout.addView(verticalSeekBar3, params);
        layout.addView(verticalSeekBar4, params);
        
        Button recordButton = (Button)findViewById(R.id.buttonRecord);
        Button playButton = (Button)findViewById(R.id.buttonPlay);
        
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
        
        prepareResources();
        Toast.makeText(FourTrack.this, "Free space: " + (int)freeSpace() + "MB", Toast.LENGTH_LONG).show();
        
        recordButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		
        		ar = AudioRecorder.getInstance(false);
        		String track = trackMap.get(radioGroup.getCheckedRadioButtonId());
        		
        		try {
        			ar.setOutputFile(trackPath + track);
        			ar.prepare();
        			ar.start();
        			playAudio(track);
        			Toast.makeText(FourTrack.this, "Recording", Toast.LENGTH_LONG).show();
        		} catch (Exception e) {
        			Log.e("record: ", e.getMessage());
        		}
	
        	}
        });
        
        playButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		
        		ar.stop();
        		ar.release();
        		prepareResources();
        		Toast.makeText(FourTrack.this, "Free space: " + (int)freeSpace() + "MB", Toast.LENGTH_LONG).show();
        		
        	}
        });
        
        
    }
    
    public void prepareResources() {
    	
    	if (sdIsMounted) {
    		try {
    			if (!(new File(trackPath)).exists()) {
    				new File(trackPath).mkdirs();
    				
    			}
    			
    			if ((new File(trackPath + "track1.wav")).exists()) {
    				track1Exists = true;
    			}
    			if ((new File(trackPath + "track2.wav")).exists()) {
    				track2Exists = true;
    			}
    			if ((new File(trackPath + "track3.wav")).exists()) {
    				track3Exists = true;
    			}
    			if ((new File(trackPath + "track4.wav")).exists()) {
    				track4Exists = true;
    			}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    }
    
    public void playAudio(String recordingTrack) {

    	
        
		if (track1Exists) {
			try {
				mp1.reset();
				mp1.setDataSource(trackPath + "track1.wav");
				mp1.prepare();
				mp1.setOnPreparedListener(new OnPreparedListener() {
					public void onPrepared(MediaPlayer mp) {

						mp1.start();
					}
				});
			} catch (IOException e) {
				Log.e(getString(R.string.app_name), e.getMessage());
			}
		}
		
		if (track2Exists) {
			try {
				mp2.reset();
				mp2.setDataSource(trackPath + "track2.wav");
				mp2.prepare();
				mp2.setOnPreparedListener(new OnPreparedListener() {
					public void onPrepared(MediaPlayer mp) {

						mp2.start();
					}
				});
			} catch (IOException e) {
				Log.e(getString(R.string.app_name), e.getMessage());
			}
		}
		
		if (track3Exists) {
			try {
				mp3.reset();
				mp3.setDataSource(trackPath + "track3.wav");
				mp3.prepare();
				mp3.setOnPreparedListener(new OnPreparedListener() {
					public void onPrepared(MediaPlayer mp) {

						mp3.start();
					}
				});
			} catch (IOException e) {
				Log.e(getString(R.string.app_name), e.getMessage());
			}
		}
		
		if (track4Exists) {
			try {
				mp4.reset();
				mp4.setDataSource(trackPath + "track4.wav");
				mp4.prepare();
				mp4.setOnPreparedListener(new OnPreparedListener() {
					public void onPrepared(MediaPlayer mp) {

						mp4.start();
					}
				});
			} catch (IOException e) {
				Log.e(getString(R.string.app_name), e.getMessage());
			}
		}
    }
    
    public double freeSpace() {
    	
    	StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
    	double availableSize = (double)stat.getAvailableBlocks() *(double)stat.getBlockSize();
    	//One binary gigabyte equals 1,073,741,824 bytes.
    	//double gigaAvailable = sdAvailSize / 1073741824;
    	return availableSize / 1048576;
    }
}